import { Person } from "../intefaces/IPerson";
import { Grade } from "../enums";
import { Faculty } from "../classes/Faculty";
import { Course } from "../classes/Course";

export type CourseGrade = {
    courseName: string;
    grade: Grade;
};

export type Teacher = Person & { specialization: string };

export type ReadonlyFaculty = Readonly<Faculty<Course>>;
