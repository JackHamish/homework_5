export function isNamedEntity(entity: unknown): entity is { name: string } {
    return typeof entity === "object" && entity !== null && "name" in entity;
}
