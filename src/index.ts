import { Course } from "./classes/Course";
import { Faculty } from "./classes/Faculty";
import { Student } from "./classes/Student";
import { Teacher } from "./classes/Teacher";
import { University } from "./classes/University";
import { FacultyName } from "./enums";
import { adder, getErrorMessage, printLine } from "./utils";

// Create university

try {
    const university = new University(
        "Horizon University",
        " Cascade Rd, Seattle",
        "Dr. Benjamin Foster"
    );

    // Create faculty

    const engineeringFaculty = new Faculty(FacultyName.Engineering, "Prof. Robert Mitchell");
    const scienceFaculty = new Faculty(FacultyName.Science, "Dr. Sarah Reynolds");
    const artsFaculty = new Faculty(FacultyName.Arts, "Dr. Jessica Turner");

    printLine(80);

    // Add faculty to university

    adder<University, Faculty<Course>>(university, university.addFaculty, [
        engineeringFaculty,
        scienceFaculty,
        artsFaculty,
    ]);

    printLine(80);

    // Create teachers
    const profRoberts = new Teacher("Prof. Jennifer Roberts", 62, "Information Technology");
    const profDavis = new Teacher("Prof. Robert Davis", 70, "Mechanical Engineering");
    const profJhonson = new Teacher("Prof. Mark Johnson", 58, "Chemistry");
    const profBaker = new Teacher("Prof. Laura Baker", 49, "Physics");
    const profParker = new Teacher("Prof. Elizabeth Parker", 58, "Art History");

    // Add teachers to faculty

    artsFaculty.addTeacher(profParker);

    adder<Faculty<Course>, Teacher>(engineeringFaculty, engineeringFaculty.addTeacher, [
        profRoberts,
        profDavis,
    ]);

    adder<Faculty<Course>, Teacher>(scienceFaculty, scienceFaculty.addTeacher, [
        profJhonson,
        profBaker,
    ]);

    printLine(80);

    // Create courses

    const infoTechCourse = new Course("Information Technology", profRoberts);
    const mechEngCourse = new Course("Mechanical Engineering", profDavis);
    const chemistryCourse = new Course("Chemistry", profJhonson);
    const physicsCourse = new Course("Physics", profBaker);
    const artHistoryCourse = new Course("Art History", profParker);

    // Add courses to faculty

    artsFaculty.addCourse(artHistoryCourse);

    adder<Faculty<Course>, Course>(engineeringFaculty, engineeringFaculty.addCourse, [
        infoTechCourse,
        mechEngCourse,
    ]);

    adder<Faculty<Course>, Course>(scienceFaculty, scienceFaculty.addCourse, [
        chemistryCourse,
        physicsCourse,
    ]);

    printLine(80);

    //Craete students

    const sophia = new Student("Sophia Davis", 19, "Engineer");
    const ethan = new Student("Ethan Roberts", 20, "Engineer");
    const leam = new Student("Liam Wilson", 21, "Engineer");
    const emma = new Student("Emma Mitchell", 19, "Engineer");
    const ava = new Student("Ava Johnson", 24, "Scientist");
    const noah = new Student("Noah Mitchell", 22, "Scientist");
    const masson = new Student("Mason Davis", 19, "Scientist");
    const olivia = new Student("Olivia Evans", 23, "Scientist");
    const daniel = new Student("Daniel Clark", 20, "Arts");

    // Add students to faculty

    artsFaculty.addStudent(daniel);

    adder<Faculty<Course>, Student>(engineeringFaculty, engineeringFaculty.addStudent, [
        sophia,
        ethan,
        leam,
        emma,
    ]);

    adder<Faculty<Course>, Student>(scienceFaculty, scienceFaculty.addStudent, [
        ava,
        noah,
        masson,
        olivia,
    ]);

    printLine(80);

    //Add students to course

    adder<Course, Student>(infoTechCourse, infoTechCourse.addStudent, [sophia, ethan]);
    adder<Course, Student>(mechEngCourse, mechEngCourse.addStudent, [leam, emma]);
    adder<Course, Student>(chemistryCourse, chemistryCourse.addStudent, [ava, noah]);
    adder<Course, Student>(physicsCourse, physicsCourse.addStudent, [masson, olivia]);

    printLine(80);

    artHistoryCourse.removeStudent(daniel.name);
    artsFaculty.removeStudent(daniel.name);
    artsFaculty.removeCourse(artHistoryCourse.name);
    artsFaculty.removeTeacher(profParker.name);
    university.removeFaculty(artsFaculty.name);

    printLine(80);

    profBaker.teachLesson([sophia, ethan], infoTechCourse.name);
    profDavis.teachLesson([leam, emma], mechEngCourse.name);
    profJhonson.teachLesson([ava, noah], chemistryCourse.name);
    profRoberts.teachLesson([masson, olivia], physicsCourse.name);

    printLine(80);

    try {
        sophia.copyHomework();
    } catch (error) {
        const message = getErrorMessage(error);
        console.log(message);
    }

    printLine(80);

    profJhonson.consultation(ava, chemistryCourse.name, "ok");
    profRoberts.consultation(masson, physicsCourse.name, "fail");

    masson.goParty();

    printLine(80);

    engineeringFaculty.getGoldMedalist();
    scienceFaculty.getGoldMedalist();
} catch (error) {
    console.log(error);
}
