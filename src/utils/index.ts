import { isNamedEntity } from "../types/typeGuards";

export function getErrorMessage(error: unknown): string {
    if (error instanceof Error) return error.message;
    return String(error);
}

export function adder<T extends object, S>(entity: T, method: (arg: S) => void, arr: S[]): void {
    if (!isNamedEntity(entity)) {
        throw new Error('Entity must have a "name" property.');
    }

    for (let i = 0; i < arr.length; i++) {
        method.call(entity, arr[i]);
    }
}

export function getRandomGrade(): number {
    return Math.floor(Math.random() * 5);
}

export function printLine(length: number): void {
    const line = "-".repeat(length);
    console.log(line);
}
