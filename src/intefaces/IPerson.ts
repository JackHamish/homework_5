import { Role } from "../enums";

export interface IPerson {
    name: string;
    age: number;
    role: Role;
}
