export interface IStudent {
    name: string;
    age: number;
    profession: string;
}
