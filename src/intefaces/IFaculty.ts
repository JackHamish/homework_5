export interface IFaculty {
    name: string;
    dean: string;
}
