export interface IUniversity {
    name: string;
    location: string;
    rector: string;
}
