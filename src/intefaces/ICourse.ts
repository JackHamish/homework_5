import { Teacher } from "../types";

export interface ICourse {
    name: string;
    teacher: Teacher;
}
