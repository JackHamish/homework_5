export function searchLogger(
    target: any,
    name: string | symbol,
    descriptor: PropertyDescriptor
): PropertyDescriptor {
    const originalMethod = descriptor.value;

    descriptor.value = function (...args: any[]) {
        console.log(`Someone wants to know about: ${args[0]}`);

        return originalMethod.apply(this, args);
    };

    return descriptor;
}

export function expelLogger(
    target: any,
    name: string | symbol,
    descriptor: PropertyDescriptor
): PropertyDescriptor {
    const originalMethod = descriptor.value;

    descriptor.value = function (...args: any[]) {
        console.log(`Unfortunately, ${args[0]} was expelled`);

        return originalMethod.apply(this, args);
    };

    return descriptor;
}

export function LogGetter(
    target: Object,
    propertyKey: string | symbol,
    descriptor: PropertyDescriptor
): void {
    const originalGetter = descriptor.get;

    descriptor.get = function (this: any): any {
        console.log(`Accessing getter: ${String(propertyKey)}`);
        return originalGetter?.apply(this);
    };
}

export function LogMood(
    target: Object,
    propertyKey: string | symbol,
    descriptor: PropertyDescriptor
): void {
    const originalSetter = descriptor.set;

    descriptor.set = function (this: any, value: any): void {
        console.log(`${this.name} feels ${value}`);
        originalSetter?.apply(this, [value]);
    };
}
