import { LogGetter, expelLogger } from "../decorators";
import { IFaculty } from "../intefaces/IFaculty";
import { Course } from "./Course";
import { Student } from "./Student";
import { Teacher } from "./Teacher";

export class Faculty<T extends Course> implements IFaculty {
    private _students: Student[] = [];
    private _teachers: Teacher[] = [];
    private _courses: T[] = [];

    constructor(public name: string, public dean: string) {}

    get students(): Student[] {
        return this._students;
    }

    @LogGetter
    get teachers(): Teacher[] {
        return this._teachers;
    }

    get courses(): T[] {
        return this._courses;
    }

    addCourse(course: T): void {
        const isExists = this.findCourse(course.name);

        if (isExists) {
            console.log(`Course with the name '${course.name}' already exists.`);
            return;
        }

        this._courses.push(course);

        console.log(`Course added: ${course.name}`);
    }

    private findCourse(name: string): Readonly<T> | undefined {
        return this._courses.find((course) => course.name === name);
    }

    removeCourse(name: string): void {
        const isExists = this.findCourse(name);

        if (!isExists) {
            console.log(`Course with the name '${name}' Not found.`);
            return;
        }
        this._courses = this._courses.filter((course) => course.name !== name);

        console.log(`Course removed: ${JSON.stringify(name)}`);
    }

    addStudent(student: Student): void {
        const isExists = this._students.some((s) => s.name === student.name);

        if (isExists) {
            console.log(`Student with the name '${student.name}' already exists.`);
            return;
        }

        this._students.push(student);

        console.log(`${student.name} enrolled in the ${this.name} faculty`);
    }

    private findStudent(name: string): Readonly<Student> | undefined {
        return this._students.find((student) => student.name === name);
    }

    @expelLogger
    removeStudent(name: string): void {
        const isExists = this.findStudent(name);

        if (!isExists) {
            console.log(`Student with the name '${name}' Not found.`);
            return;
        }

        this._students = this._students.filter((student) => student.name !== name);
    }

    addTeacher(teacher: Teacher): void {
        const isExists = this.findTeacher(teacher.name);

        if (isExists) {
            console.log(`Teacher with the name '${teacher.name}' already exists.`);
            return;
        }

        this._teachers.push(teacher);

        console.log(`${teacher.name} started working at the ${this.name} faculty`);
    }

    private findTeacher(name: string): Teacher | undefined {
        return this._teachers.find((teacher) => teacher.name === name);
    }

    @expelLogger
    removeTeacher(name: string): void {
        const isExists = this.findTeacher(name);

        if (!isExists) {
            console.log(`Teacher with the name '${name}' Not found.`);
            return;
        }

        this._teachers = this._teachers.filter((teacher) => teacher.name !== name);
    }

    private calculateAvgGrade(student: Student): number {
        const totalGrade: number = student.grades.reduce((acc, val) => acc + val.grade, 0);

        const avgGrade: number = totalGrade / student.grades.length;

        return avgGrade;
    }

    private findGoldMedalist(): Student | undefined {
        let avgGrade = 0;
        let goldStudent: Student | undefined;
        const minAvgGrade: number = 4;

        this._students.forEach((student) => {
            let averageGrade = this.calculateAvgGrade(student);

            if (averageGrade > avgGrade && averageGrade >= minAvgGrade) {
                avgGrade = averageGrade;
                goldStudent = student;
            }
        });

        return goldStudent;
    }

    public getGoldMedalist(): void {
        let goldMedalist = this.findGoldMedalist();

        if (goldMedalist) {
            console.log(`On ${this.name} faculty gold medalist is: ${goldMedalist.name}`);
        } else {
            console.log(`Unfortunately there are no gold medalists`);
        }
    }
}
