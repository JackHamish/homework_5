import { Grade, Mood, Role } from "../enums";
import { CourseGrade } from "../types";
import { getRandomGrade, printLine } from "../utils";
import { Person } from "./Person";
import { Student } from "./Student";

export class Teacher extends Person {
    constructor(public name: string, public age: number, protected specialization: string) {
        super(name, age, Role.Teacher);
    }

    private evaluateStudent(student: Student, grade: CourseGrade): void {
        student.grades.push(grade);

        console.log(
            `${student.name} received a grade: ${grade.grade} per course: ${grade.courseName}`
        );

        if (grade.grade >= 4) {
            student.moodValue = Mood.Happy;
        } else {
            student.moodValue = Mood.Bad;
        }
    }

    private improveStudentScore(student: Student, courseName: string, score: Grade): void {
        const updatedGrade = student.grades.find((grade) => grade.courseName === courseName);

        if (updatedGrade) updatedGrade.grade = score;

        console.log(
            `${student.name} improove a grade: ${updatedGrade?.grade} per course: ${courseName}`
        );
    }

    consultation(student: Student, courseName: string, result: "ok" | "fail"): void {
        console.log(`${this.name} advises ${student.name} on a ${courseName} course`);

        printLine(5);

        if (result === "ok") {
            console.log(`${student.name} is now a ${courseName} guru`);
            this.improveStudentScore(student, courseName, 5);
            student.moodValue = Mood.Happy;

            printLine(13);
        } else {
            console.log(`${student.name} failed the retake`);
            student.moodValue = Mood.Bad;
            printLine(13);
        }
    }

    teachLesson(students: Student[], corseName: string): void {
        console.log(`${this.name} gives a lecture on ${corseName}`);

        printLine(5);

        students.forEach((student) =>
            this.evaluateStudent(student, { courseName: corseName, grade: getRandomGrade() })
        );

        printLine(13);
    }
}
