import { LogMood } from "../decorators";
import { Mood, Role } from "../enums";
import { IPerson } from "../intefaces/IPerson";

export abstract class Person implements IPerson {
    private _mood: Mood;

    constructor(public name: string, public age: number, public role: Role) {
        this._mood = Mood.Happy;
    }

    @LogMood
    set moodValue(mood: Mood) {
        this._mood = mood;
    }

    get mood(): Mood {
        return this._mood;
    }

    presentation(): void {
        console.log(
            `Hello, my name is ${this.name}. I am ${this.age} years old. I am a ${this.role}`
        );
    }
}
