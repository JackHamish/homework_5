import { expelLogger } from "../decorators";
import { ICourse } from "../intefaces/ICourse";
import { Teacher } from "../types";
import { Student } from "./Student";

export class Course implements ICourse {
    private _students: Student[] = [];

    constructor(public name: string, public teacher: Teacher) {}

    set newTeacher(teacher: Teacher) {
        this.teacher = teacher;
    }

    addStudent(student: Student): void {
        const isExists = this._students.some((s) => s.name === student.name);

        if (isExists) {
            console.log(`Student with the name '${student.name}' already exists.`);
            return;
        }

        this._students.push(student);

        console.log(`${student.name} is studying ${this.name}`);
    }

    @expelLogger
    removeStudent(name: string): void {
        const isExists = this._students.some((s) => s.name === name);

        if (!isExists) {
            console.log(`Student with the name '${name}' Not found.`);
            return;
        }
        this._students = this._students.filter((student) => student.name !== name);
    }

    getStudents(): Student[] {
        return this._students;
    }

    removeTeacher(): void {
        this.teacher = null;

        console.log(`Unfortunately, ${this.teacher} was expelled`);
    }
}
