import { LogGetter } from "../decorators";
import { IUniversity } from "../intefaces/IUniversity";
import { ReadonlyFaculty } from "../types";
import { Course } from "./Course";
import { Faculty } from "./Faculty";

export class University implements IUniversity {
    private _faculties: Faculty<Course>[] = [];

    constructor(public name: string, public location: string, public rector: string) {}

    @LogGetter
    get faculties(): ReadonlyFaculty[] {
        return this._faculties;
    }

    private findFaculty(name: string): ReadonlyFaculty | undefined {
        return this._faculties.find((faculty) => faculty.name === name);
    }

    addFaculty(faculty: Faculty<Course>): void {
        const isExists = this.findFaculty(faculty.name);

        if (isExists) {
            console.log(`Faculty with the name '${faculty.name}' already exists.`);
            return;
        }

        this._faculties.push(faculty);

        console.log(`Faculty added: ${JSON.stringify(faculty)}`);
    }

    removeFaculty(facultyName: string): void {
        const faculty = this.findFaculty(facultyName);

        if (!faculty) {
            console.log(`Faculty with the name '${facultyName}' not found.`);
            return;
        }

        this._faculties = this._faculties.filter((faculty) => faculty.name !== facultyName);

        console.log(`Faculty removed: ${JSON.stringify(facultyName)}`);
    }
}
