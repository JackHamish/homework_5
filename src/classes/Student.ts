import { Mood, Role } from "../enums";
import { IStudent } from "../intefaces/IStudent";
import { CourseGrade } from "../types";
import { Person } from "./Person";

export class Student extends Person implements IStudent {
    public grades: CourseGrade[] = [];

    constructor(public name: string, public age: number, public profession: string) {
        super(name, age, Role.Student);
    }

    getGrades(): CourseGrade[] {
        return this.grades;
    }

    copyHomework(): never {
        throw new Error(`The teacher found out that ${this.name} cheated`);
    }

    goParty() {
        console.log(`${this.name} went to a party`);
        this.moodValue = Mood.Happy;
    }
}
