export enum Role {
    Student = "Student",
    Teacher = "Teacher",
}

export enum Grade {
    A = 5,
    B = 4,
    C = 3,
    D = 2,
    F = 1,
}

export enum Mood {
    Happy = "Happy",
    Bad = "Bad",
}

export enum FacultyName {
    Science = "Science",
    Engineering = "Engineering",
    Arts = "Arts",
}
